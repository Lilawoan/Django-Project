from django.urls import reverse
from django.test import TestCase
from django.urls.exceptions import NoReverseMatch
from django.template.loader import render_to_string
from django.core.exceptions import ObjectDoesNotExist

from musiques.models import Morceau, Artiste

# Create your tests here.


class MorceauTestCase(TestCase):
    def setUp(self):
        Artiste.objects.create(nom='artiste1')
        artiste = Artiste.objects.get(nom='artiste1')

        Morceau.objects.create(titre='musique1', artiste_fk=artiste)
        Morceau.objects.create(titre='musique2', artiste_fk=artiste)
        Morceau.objects.create(titre='musique-delete', artiste_fk=artiste)

    def test_morceau_url_name(self):
        """
        Vérifie l'existence des routes detail, create, liste, update, delete
        """
        try:
            url = reverse('musiques:morceau-detail', args=[1])
        except NoReverseMatch:
            assert False

        try:
            url = reverse('musiques:morceau-create')
        except NoReverseMatch:
            assert False

        try:
            url = reverse('musiques:morceau-update', args=[1])
        except NoReverseMatch:
            assert False
        try:
            url = reverse('musiques:morceau-liste')
        except NoReverseMatch:
            assert False
        try:
            url = reverse('musiques:morceau-delete')
        except NoReverseMatch:
            assert False

    def test_morceau_url_detail(self):
        """
        Test la route detail
        """
        morceau = Morceau.objects.get(titre='musique1')
        url = reverse('musiques:morceau-detail', args=[morceau.pk])
        response = self.client.get(url)
        assert response.status_code == 200

    def test_morceau_url_create(self):
        """
        Test la route create
        """
        url = reverse('musiques:morceau-create')
        response = self.client.get(url)
        assert response.status_code == 200

    def test_morceau_url_update(self):
        """
        Test la route update
        """
        morceau = Morceau.objects.get(titre='musique1')
        url = reverse('musiques:morceau-update', args=[morceau.pk])
        response = self.client.get(url)
        assert response.status_code == 200

    def test_morceau_url_liste(self):
        """
        Test la route liste
        """
        url = reverse('musiques:morceau-liste')
        response = self.client.get(url)
        assert response.status_code == 200

    def test_morceau_delete(self):
        """
        Test la route de suppression
        """
        morceau = Morceau.objects.get(titre='musique-delete')
        url = reverse('musiques:morceau-delete', args=[morceau.pk])
        response = self.client.get(url)
        try:
            morceau = Morceau.objects.get(pk=morceau.pk)
        except ObjectDoesNotExist:
            assert True
            return
        assert False


class ArtisteTestCase(TestCase):
    def setUp(self):
        Artiste.objects.create(nom='artiste1')
        Artiste.objects.create(nom='artiste-delete')

    def test_artiste_urls(self):
        """
        Vérifie l'existence des routes detail, create, liste, update, delete
        """
        try:
            url = reverse('musiques:artiste-detail', args=[1])
        except NoReverseMatch:
            assert False
        try:
            url = reverse('musiques:artiste-create')
        except NoReverseMatch:
            assert False
        try:
            url = reverse('musiques:artiste-update', args=[1])
        except NoReverseMatch:
            assert False
        try:
            url = reverse('musiques:artiste-liste')
        except NoReverseMatch:
            assert False
        try:
            url = reverse('musiques:artiste-delete')
        except NoReverseMatch:
            assert False

    def test_artiste_url_detail(self):
        """
        Test la route detail
        """
        artiste = Artiste.objects.get(nom='artiste1')
        url = reverse('musiques:artiste-detail', args=[artiste.pk])
        response = self.client.get(url)
        assert response.status_code == 200

    def test_artiste_url_create(self):
        """
        Test la route create
        """
        url = reverse('musiques:artiste-create')
        response = self.client.get(url)
        assert response.status_code == 200

    def test_artiste_url_update(self):
        """
        Test la route update
        """
        artiste = Artiste.objects.get(nom='artiste1')
        url = reverse('musiques:artiste-update', args=[artiste.pk])
        response = self.client.get(url)
        assert response.status_code == 200

    def test_artiste_url_liste(self):
        """
        Test la route liste
        """
        url = reverse('musiques:artiste-liste')
        response = self.client.get(url)
        assert response.status_code == 200

    def test_artiste_delete(self):
        """
        Test la route de suppression
        """
        artiste = Artiste.objects.get(nom='artiste-delete')
        url = reverse('musiques:artiste-delete', args=[artiste.pk])
        response = self.client.get(url)
        try:
            artiste = Artiste.objects.get(pk=artiste.pk)
        except ObjectDoesNotExist:
            assert True
            return
        assert False
