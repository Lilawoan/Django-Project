# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-05 09:56
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('musiques', '0006_auto_20171005_0951'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='morceau',
            name='artiste',
        ),
        migrations.AlterField(
            model_name='morceau',
            name='artiste_fk',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='musiques.Artiste'),
        ),
    ]
