# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-05 08:53
from __future__ import unicode_literals

from django.db import migrations


def lier_fk(apps, schema):
    Morceau = apps.get_model('musiques', 'Morceau')
    Artiste = apps.get_model('musiques', 'Artiste')

    lesArtistes = Artiste.objects.all()
    lesMorceau = Morceau.objects.all()

    # for artiste, morceau in lesArtistes, lesMorceau:
    #     if artiste.nom == morceau.artiste:
    #         morceau.artiste_fk = artiste.id

    for artiste in lesArtistes:
        for morceau in lesMorceau:
            if artiste.nom == morceau.artiste:
                morceau.artiste_fk = artiste
                morceau.save()
    # for morceau in lesMorceau:
    #     artiste = Artiste.objects.get(nom=morceau.artiste)
    #     morceau.artiste_fk = artiste
    #     morceau.save()


def annuler_lier_fk(apps, schema):
    Morceau = apps.get_model('musiques', 'Morceau')
    Morceau.objects.all().update(artiste_fk=None)


class Migration(migrations.Migration):

    dependencies = [
        ('musiques', '0005_morceau_artiste_fk'),
    ]

    operations = [
        migrations.RunPython(lier_fk, reverse_code=annuler_lier_fk)
    ]
