from django.conf.urls import url
from .views import *

app_name = 'musiques'
urlpatterns = [
    # Urls des musiques
    url(r'^create/$',
        MorceauCreateView.as_view(), name='morceau-create'),
    url(r'^(?P<pk>\d+)$',
        MorceauDetailView.as_view(), name='morceau-detail'),
    url(r'^liste/$',
        MorceauListView.as_view(), name='morceau-liste'),
    url(r'^update/(?P<pk>\d+)$',
        MorceauUpdateView.as_view(), name='morceau-update'),
    url(r'^delete/(?P<id>\d+)$', deleteMorceau, name='morceau-delete'),
    # Urls de l'artiste
    url(r'^artiste/create/$',
        ArtisteCreateView.as_view(), name='artiste-create'),
    url(r'^artiste/(?P<pk>\d+)',
        ArtisteDetailView.as_view(), name='artiste-detail'),
    url(r'^artiste/update/(?P<pk>\d+)',
        ArtisteUpdateView.as_view(), name='artiste-update'),
    url(r'^artiste/liste/$',
        ArtisteListView.as_view(), name='artiste-liste'),
    url(r'^artiste/delete/(?P<id>\d+)$', deleteArtiste, name='artiste-delete')
]
