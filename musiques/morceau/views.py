from django.shortcuts import render, reverse, redirect
from django.http import HttpResponse
from django.views.generic import DetailView, CreateView, UpdateView, ListView
from musiques.models import Morceau


class MorceauCreateView(CreateView):
    """
    Définition de la vue pour créer un morceau à partir d'une vue générique
    """
    model = Morceau
    fields = ['titre', 'artiste_fk', 'date_sortie']


class MorceauDetailView(DetailView):
    """
    Définition de la vue pour afficher les détails d'un morceau
    """
    model = Morceau


class MorceauUpdateView (UpdateView):
    """
    Définition de la vue pour modifier un morceau à partir d'une vue générique
    """
    model = Morceau
    fields = ['titre', 'artiste_fk', 'date_sortie']


class MorceauListView(ListView):
    model = Morceau


def deleteMorceau(request, id):
    Morceau.objects.filter(id=id).delete()
    return redirect(reverse('musiques:morceau-liste'))
