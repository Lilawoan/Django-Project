from django.db import models
from musiques.artistes.model import Artiste
from django.urls import reverse


class Morceau(models.Model):
    """
    Définition d'un morceau de musique
    """
    titre = models.CharField(max_length=64)
    date_sortie = models.DateField(null=True)
    artiste_fk = models.ForeignKey(Artiste)

    def get_absolute_url(self):
        """
        url pour la redirection après création d'un objet
        """
        return reverse('musiques:morceau-detail', kwargs={'pk': self.pk})

    def __str__(self):
        return '{self.titre}' . format(self=self)
