from django.contrib import admin
from musiques.models import Morceau, Artiste

# Register your models here.


class MorceauAdmin(admin.ModelAdmin):
    list_display = ('titre', 'artiste_fk')


admin.site.register(Morceau, MorceauAdmin)


class ArtisteAdmin(admin.ModelAdmin):
    list_display = ('nom', 'nom')


admin.site.register(Artiste, ArtisteAdmin)
