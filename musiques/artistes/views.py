from django.shortcuts import render, redirect, reverse
from django.http import HttpResponse
from django.views.generic import DetailView, CreateView, UpdateView, ListView
from musiques.models import Artiste


class ArtisteCreateView(CreateView):
    """
    Définition de la vue pour créer un artiste à partir d'une vue générique
    """
    model = Artiste
    fields = ['nom']


class ArtisteDetailView(DetailView):
    """
    Définition de la vue pour afficher les détails d'un artiste
    """
    model = Artiste


class ArtisteUpdateView(UpdateView):
    model = Artiste
    fields = ['nom']


class ArtisteListView(ListView):

    model = Artiste


def deleteArtiste(request, id):
    Artiste.objects.filter(id=id).delete()
    return redirect(reverse('musiques:artiste-liste'))
