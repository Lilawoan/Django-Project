from django.db import models
from django.urls import reverse


class Artiste(models.Model):
    """
    Définition d'un artiste pour l'application django
    """
    nom = models.CharField(max_length=64)

    def get_absolute_url(self):
        """
        url pour la redirection après création d'un objet
        """
        return reverse('musiques:artiste-detail', kwargs={'pk': self.pk})

    def __str__(self):
        return '{self.nom}' . format(self=self)
